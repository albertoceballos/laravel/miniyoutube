@extends('layouts.app')

@section('content')
<div class="container">
    @if(session('message'))
    <div class="alert alert-success text-center">{{session('message')}}</div>
    @endif

    @include('video.videos-list')
</div>
@endsection
