@extends('layouts.app')
@section('content')
<div class="container">
    <h2>Canal de {{$user->name.' '.$user->surname}}</h2>
    <hr>
    @include('video.videos-list')
</div>

@endsection
