<div class="col-8 mb-3">
    <h3>Añade tu comentario</h3>
    <hr>
    <form method="POST" action="{{url('/comments/store')}}">
        @csrf
        <input type="hidden" name="video_id" id="video_id" value="{{$video->id}}">
        <div class="form-group">
            <textarea name="body" id="body" class="form-control" placeholder="Añade tu cometario"></textarea>
        </div>
        <div class="form-group">
            <input type="submit" value="Añadir Comentario" class="btn btn-success">
        </div>
    </form>
</div>
 @if(session('message'))
      <div class="col-10 mx-auto alert alert-success text-center">{{session('message')}}</div>
 @endif
<div class="col-12 mx-1">
    <h2>Comentarios</h2>
    <hr>
    @foreach($video->comments as $comment)

    <div class="card my-2">
        <div class="card-header">
            <span class="font-weight-bold">{{$comment->user->name." ".$comment->user->surname}}</span> {{\FormatTime::LongTimeFilter($comment->created_at)}}
        </div>
        <div class="card-body">
            <p class="card-text">{{$comment->body}}</p>
            @if(Auth::user()->id==$comment->user->id )
            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteCommentModal{{$comment->id}}">Borrar comentario</button>
            
            <!-- Modal -->
            <div class="modal fade" id="deleteCommentModal{{$comment->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">¿Seguro que quieres borrar este comentario?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            {{$comment->body}}
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                            <a href="{{route('deleteComment',$comment->id)}}" class="btn btn-danger">Borrar</a>
                        </div>
                    </div>
                </div>
            </div>

            @endif
        </div>
    </div>
    @endforeach
</div> 
