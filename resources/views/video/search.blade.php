@extends('layouts.app')
@section('content')
<div class="col-8 mx-auto">
    <h2>Búsqueda: "{{$busqueda}}"</h2>
    <hr>
    <div class="d-flex justify-content-start">
    <form action="{{url('videos/search/'.$busqueda)}}" method="GET">
        <select name="filter" id="">
            <option value="new">Más nuevos primero</option>
            <option value="old">Más viejos primero</option>
            <option value="alfa_asc">por título A-Z</option>
            <option value="alfa_desc">por título Z-A</option>
        </select>
        <input type="submit" value="Ordenar" class="btn btn-primary">
    </form>
    </div>
    @include('video.videos-list')
</div>
@endsection
