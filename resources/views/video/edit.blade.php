@extends('layouts/app')

@section('content')
<div class="col-7 mx-auto">
    <h1>Editar vídeo</h1>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{route('updateVideo',$video->id)}}" method="POST" enctype="multipart/form-data" class="col-10">
        @csrf
        <div class="form-group">
            <label for="title">Título</label>
            <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror" value="{{$video->title}}">
        </div>
        <div class="form-group">
            <label for="description">Descripción</label>
            <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror">{{$video->description}}</textarea>
        </div>
        <div class="form-group">
            <label for="image">Miniatura</label><br>
            <img src="{{route('imageVideo',$video->image)}}" alt="" class="col-4 mt-3">
            <input type="file" id="image" name="image" class="form-control">
        </div>

        <div class="form-group">
            <label for="path">Vídeo</label><br>
            <video controls class="col-6 mt-3">
                <source src="{{route('videoFile',$video->path)}}"></source>
            </video>
            <input type="file" id="path" name="path" class="form-control @error('path') is-invalid @enderror">
        </div>
        <div class="form-group">
            <input type="submit" value="Guardar cambios" class="btn btn-lg btn-success">
        </div>
    </form>

</div>
@endsection
