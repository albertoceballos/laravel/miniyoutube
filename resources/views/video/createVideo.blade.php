@extends('layouts/app')

@section('content')
<div class="col-7 mx-auto">
    <h1>Crear vídeo</h1>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach($errors->all() as $error)
            <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif

    <form action="{{route('saveVideo')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="title">Título</label>
            <input type="text" id="title" name="title" class="form-control @error('title') is-invalid @enderror">
        </div>
        <div class="form-group">
            <label for="description">Descripción</label>
            <textarea id="description" name="description" class="form-control @error('description') is-invalid @enderror"></textarea>
        </div>
        <div class="form-group">
            <label for="image">Miniatura</label>
            <input type="file" id="image" name="image" class="form-control">
        </div>
        <div class="form-group">
            <label for="path">Ruta</label>
            <input type="file" id="path" name="path" class="form-control @error('path') is-invalid @enderror">
        </div>
        <div class="form-group">
            <input type="submit" value="Crear vídeo" class="btn btn-lg btn-success">
        </div>
    </form>

</div>
@endsection