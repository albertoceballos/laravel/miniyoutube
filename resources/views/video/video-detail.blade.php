@extends('layouts.app')

@section('content')

<div class="col-8 offset-2">
    <h2>{{$video->title}}</h2>
    <hr>
    <div class="card mx-auto" style="width: 50rem">
        <video controls class="col-12 mt-3">
            <source src="{{route('videoFile',$video->path)}}"></source>
        </video>
        <div class="card-body">
            <p class="card-text">{{$video->description}}</p>
            <p class="text-muted">Creado por <a href="{{route('userChannel',$video->user->id)}}">{{$video->user->name." ".$video->user->surname}}</a> {{\FormatTime::LongTimeFilter($video->created_at)}}</p>
        </div>
        @include('video.comments')
    </div>
</div>
@endsection