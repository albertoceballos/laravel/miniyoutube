@if(count($videos)>0)
<div class="row justify-content-center">    
    @foreach($videos as $video)
    <div class="card col-10 my-2">
        <div class="card-body d-flex">
            <div class="">
                @if(Storage::disk('images')->has($video->image))
                <img src="{{route('imageVideo',$video->image)}}" alt="" width="200px" />
                @endif
            </div>
            <div class="">
                <ul style="list-style: none">
                    <li class="video-title">  {{$video->title}}</li>
                    <li ><a href="{{route('userChannel',$video->user->id)}}" class="font-weight-bold">{{$video->user->name." ".$video->user->surname}}</a> {{\FormatTime::LongTimeFilter($video->created_at)}}</li>
                </ul>


                <div class="video-edit-buttons">
                    <a href="{{route('detailVideo',$video->id)}}" class="btn btn-success">Leer</a>
                    <!--Si el usuario está logueado y coincide con el usuario del vídeo, mostrar botones edición y borrado -->   
                    @if(Auth::check() && Auth::user()->id==$video->user->id)
                    <a href="{{route('editVideo',$video->id)}}" class="btn btn-warning ml-3">Editar</a>
                    <button type="button" class="btn btn-danger ml-3" data-toggle="modal" data-target="#deleteVideoModal{{$video->id}}">Borrar</button>
                    @endif
                </div>
                <!-- Modal -->
                <div class="modal fade" id="deleteVideoModal{{$video->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">¿Seguro que quieres borrar el vídeo?</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                El vídeo "{{$video->title}}" se borrará definitivamente ¿Estás seguro?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                <a href="{{route('deleteVideo',$video->id)}}" class="btn btn-danger">Sí, borrar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
    @endforeach

</div>
<div class="row justify-content-center">
    {{$videos->links()}}           
</div>
@else
<div class="alert alert-warning col-8 text-center">No se encontraron vídeos</div>
@endif

