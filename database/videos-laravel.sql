-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-05-2019 a las 11:08:11
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `videos-laravel`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `video_id` int(11) NOT NULL,
  `body` text COLLATE utf8_spanish_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `video_id`, `body`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 'Increible el vídeo.!!!', '2019-05-09 15:59:29', '2019-05-09 15:59:29'),
(3, 1, 6, 'Mirad que pequeña que es La Tierra', '2019-05-09 16:06:24', '2019-05-09 16:06:24'),
(4, 1, 6, 'Que grande es el universo', '2019-05-09 16:07:17', '2019-05-09 16:07:17'),
(6, 1, 6, 'EL universo es increible', '2019-05-09 16:12:37', '2019-05-09 16:12:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `surname`, `email`, `password`, `image`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, NULL, 'Alberto', NULL, 'berto@berto.com', '$2y$10$hCH8K67GoSjXBwTJsxvic.4YPWOGv1iugKaaXFbLxN6fGWhImRLDu', NULL, '2019-05-08 09:13:26', '2019-05-08 09:13:26', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `description` text COLLATE utf8_spanish_ci,
  `status` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_spanish_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_spanish_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`id`, `user_id`, `title`, `description`, `status`, `image`, `path`, `created_at`, `updated_at`) VALUES
(2, 1, 'Ballenas', 'Ballenas en los océanos', NULL, '1557335043ballenas.png', '1557335043Humpback Whales - Narrated by Ewan McGregor - Official IMAX Trailer - 4K.mp4', '2019-05-08 17:04:03', '2019-05-08 17:04:03'),
(3, 1, 'Space shuttle', 'Lanzamiento transbordador espacial', NULL, '1557386138space_shuttle.png', '1557386138[HD] IMAX -- Shuttle launch (Hubble 2010 - STS 125) - Excellent Quality.mp4', '2019-05-09 07:15:38', '2019-05-09 07:15:38'),
(4, 1, 'Caza Mig-29', 'Espectacular despegue caza Mig-29', NULL, '1557386450mig29.png', '1557386450Spectacular vertical take off MIG 29 at RIAT 2015.mp4', '2019-05-09 07:20:51', '2019-05-09 07:20:51'),
(5, 1, 'Salto desde el espacio', 'Salto en caida libre desde los límites del espacio', NULL, '1557388289space jump.png', '1557388289Jumping From Space! - Red Bull Space Dive - BBC.mp4', '2019-05-09 07:51:29', '2019-05-09 07:51:29'),
(6, 1, 'Comparación de los astros del universo', 'Increible recreación en 3D comparando planetas y astros del universo!', NULL, '1557486870universe.png', '1557487091Universe Size Comparison 3D.mp4', '2019-05-09 07:56:44', '2019-05-10 11:18:11'),
(9, 1, 'Papá cuéntame otra vez', 'Videoclip de Ismael Serrano', NULL, '1557478185ismael_serrano.png', '1557478185ismael_serrano1.mp4', '2019-05-10 08:49:45', '2019-05-10 08:49:45');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `video_id` (`video_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`);

--
-- Filtros para la tabla `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `videos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
