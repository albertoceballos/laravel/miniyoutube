<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

//Controlador VideoController
Route::group(['prefix'=>'videos'], function (){
    
    //vista crear vídeo
    Route::get('/crear',[
        'as'=>'createVideo',
        'middleware'=>'auth',
        'uses'=>'VideoController@createVideo',
    ]);
    
    //guardar video
    Route::post('/guardar',[
        'as'=>'saveVideo',
        'middleware'=>'auth',
        'uses'=>'VideoController@saveVideo',
    ]);
    
    //conseguir imagen del vídeo del disco
    Route::get('/miniatura/{filename}',[
        'as'=>'imageVideo',
        'uses'=>'VideoController@getImage',
    ]);
    
    //vista detalle del vídeo
    Route::get('/video-detail/{id}',[
       'as'=>'detailVideo',
       'uses'=>'VideoController@getVideoPage',
    ]);
    
    //conseguir archivo de video del disco
    Route::get('/video-file/{filename}',[
        'as'=>'videoFile',
        'uses'=>'VideoController@getVideo',
    ]);
    
    //borrar vídeo
    Route::get('/delete/{videoId}',[
       'as'=>'deleteVideo',
       'uses'=>'VideoController@deleteVideo',
    ]);
    
    //vista de actualizar vídeo
    Route::get('/edit/{videoId}',[
       'as'=>'editVideo',
       'uses'=>'VideoController@editVideo', 
    ]);
    
    //actualizar vídeo
    Route::post('/update/{videoId}',[
       'as'=>'updateVideo',
       'uses'=>'VideoController@updateVideo',
    ]);
    
    //buscar vídeo
    Route::get('/search/{searchString?}/{filter?}',[
        'as'=>'search',
        'uses'=>'VideoController@searchVideo',
    ]);
    
});

//Controlador CommentController

Route::group(['prefix'=>'comments'],function(){
    
    //guardar nuevo comentario
    Route::post('/store',[
       'as'=>'storeComment',
       'uses'=>'CommentController@store',
    ]);
    
    //borrar comentario
    Route::get('/delete/{commentId}',[
       'as'=>'deleteComment',
       'middleware'=>'auth',
       'uses'=>'CommentController@delete', 
    ]);
});

//controlador UserCOntroller
Route::get('/channel/{userId}',[
   'as'=>'userChannel',
   'middleware'=>'auth',
   'uses'=>'UserController@channel',
]);

 


