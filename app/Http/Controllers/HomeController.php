<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//modelos
use App\Video;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        $videos= Video::orderBy('id','DESC')->paginate(3);
        return view('home',[
            'videos'=>$videos,
        ]);
    }
}
