<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;
//modelos
use App\Video;
use App\Comment;

class VideoController extends Controller {

    public function createVideo() {
        return view('video.createVideo');
    }

    public function saveVideo(Request $request) {
        $validatedData = $request->validate([
            'title' => 'required|min:5|max:255',
            'description' => 'required',
            'image' => 'mimes:jpg,jpeg,gif,png',
            'path' => 'required|mimes:mp4,mpg,mpeg',
        ]);

        $video = new Video();
        $user = \Auth::user();
        $video->user_id = $user->id;
        $video->title = $request->input('title');
        $video->description = $request->input('description');

        //subida de la imagen:
        $image = $request->file('image');
        if ($image) {
            //conseguir nombre del fichero
            $image_path = time() . $image->getClientOriginalName();
            //guardar imagen en disco
            $save_image = $request->file('image')->storeAs('images', $image_path);
            $video->image = $image_path;
        }
        //subida del vídeo:
        //conseguir archivo del request
        $video_file = $request->file('path');
        //extraer el nombre del archivo
        $video_file_path = time() . $video_file->getClientOriginalName();
        //guardar video en disco
        $request->file('path')->storeAs('videos', $video_file_path);
        $video->path = $video_file_path;

        //Guardar objeto en BBDD
        $video->save();
        return redirect()->route('home')->with('message', 'Nuevo vídeo guardado con éxito ');
    }

    //devolver una imagen para mostrar
    public function getImage($filename) {

        $file = Storage::disk('images')->get($filename);
        return new Response($file);
    }

    //vista detalle de vídeo
    public function getVideoPage($id) {

        $videos = Video::all();
        $video = $videos->find($id);

        return view('video.video-detail', [
            'video' => $video,
        ]);
    }

    //devolver video para mostrar
    public function getVideo($filename) {
        $file = Storage::disk('videos')->get($filename);
        return new Response($file);
    }

    //borrar vídeo
    public function deleteVideo($videoId) {
        //consigo usuario logueado
        $user = \Auth::user();

        //consigo video a borrar
        $video = Video::find($videoId);

        if ($user && $user->id == $video->user_id) {

            //consigo comentarios del vídeo a borrar
            $comments = Comment::where('video_id', $videoId)->get();

            //si hay comentarios los borro
            if ($comments && count($comments) > 0) {
                foreach ($comments as $comment) {
                    $comment->delete();
                }
            }

            //busco el archivo de imagen y el archivo de vídeo en disco y los borro si existen

            if (Storage::disk('images')->exists($video->image)) {
                Storage::disk('images')->delete($video->image);
            }
            if (Storage::disk('videos')->get($video->path)) {
                Storage::disk('videos')->delete($video->path);
            }

            //borrar registo de vídeo la BBDD
            $delete = $video->delete();

            if ($delete) {
                $message = "Vídeo borrado correctamente";
            } else {
                $message = "No se pudo borrar el vídeo";
            }
        } else {
            $message = "No tienes autorización para acceder";
        }

        return redirect()->route('home')->with('message', $message);
    }

    //vista de edición de vídeo
    public function editVideo($videoId) {
        $user = \Auth::user();
        $video = Video::findOrFail($videoId);

        //comprobar usuario, si es el usuario que ha publicado el vídeo mostar vista de edición sino te redirige a home
        if ($user && $user->id == $video->user_id) {
            return view('video/edit', [
                'video' => $video,
            ]);
        } else {
            return redirect()->route('home');
        }
    }

    //Actualizar vídeo
    public function updateVideo($videoId, Request $request) {

        $validatedData = $request->validate([
            'title' => 'required|min:5|max:255',
            'description' => 'required',
            'image' => 'mimes:jpg,jpeg,gif,png',
            'path' => 'mimes:mp4,mpg,mpeg',
        ]);

        $video = Video::find($videoId);
        $user = \Auth::user();

        $video->title = $request->input('title');
        $video->description = $request->input('description');


        //si existe la imagen, le asigno nombre y guardo la imagen en disco y en la BBDD
        if ($request->file('image')) {
            $image_path = time() . $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('images', $image_path);

            //borrar imagen antigua:
            if (Storage::disk('images')->exists($video->image)) {
                Storage::disk('images')->delete($video->image);
            }
            //asignar nueva ruta de imagen al objeto
            $video->image = $image_path;
        }

        //lo mismo para el vídeo
        if ($request->file('path')) {
            $video_path = time() . $request->file('path')->getClientOriginalName();
            $request->file('path')->storeAs('videos', $video_path);
            //borrar video antiguo
            if (Storage::disk('videos')->exists($video->path)) {
                Storage::disk('videos')->delete($video->path);
            }

            //asignar nueva ruta de imagen
            $video->path = $video_path;
        }

        //guardar objeto
        $video->save();
        //redireccionar a la home
        return redirect()->route('home')->with('message', 'El vídeo se ha actualizado correctamente');
    }
    
    //búsqueda de vídeos
    public function searchVideo(Request $request,$searchString=null,$filter=null){
        
        //para hacer la búsqueda mediante la url limpia sin los parámetros GET (necesario para hacer paginación correctamente)
        if(empty($searchString)){
            //cojo el parámetro que me llega por GET
            $searchString=$request->get('searchString');
            
            //si hago una búsqueda vacía me lleva a la home
            if(empty($searchString)){
                return redirect()->route('home');
               
            }
            //redirijo con el parámetro de la búsqueda
            return redirect()->route('search',$searchString);
        }
        
        If(!empty($searchString) && empty($filter) && \Request::get('filter')){
            $filter=$request->get('filter');
            
            return redirect()->route('search',['searchString'=>$searchString,'filter'=>$filter]);
        }
        
        
        
        switch ($filter){
            case 'new':
                $columna='id';
                $order='DESC';
                break;
            case 'old':
                $columna='id';
                $order='ASC';
                break;
            case 'alfa_asc':
                $columna='title';
                $order='ASC';
                break;
            case 'alfa_desc':
                $columna='title';
                $order='DESC';
                break;
            default :
                $columna='id';
                $order='DESC';
                
        }
        
        $videos= Video::where('title','LIKE','%'.$searchString.'%')->orWhere('description','LIKE','%'.$searchString.'%')->orderBy($columna,$order) ->paginate(5);
        
        return view('video.search',[
            'videos'=>$videos,
            'busqueda'=>$searchString,
        ]); 
    }

}
