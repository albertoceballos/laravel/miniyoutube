<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Video;

class UserController extends Controller
{
    public function channel($userId){
        $user= User::find($userId);
        //si el usuario no existe me redirige a la home
        if(empty($user)){
            return redirect()->route('home');
        }
        $videos= Video::where('user_id',$userId)->paginate(5);
        
        return view('user.channel',[
            'user'=>$user,
            'videos'=>$videos,
        ]);
    }
}
