<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//modelo
use App\Comment;

class CommentController extends Controller
{
    //guardar comentario
    public function store(Request $request){
        
        //validación del campo de comentario
        $validatedData=$request->validate([
           'body'=>'required', 
        ]);
        
        //crear nuevo objeto comentario
        $comment=new Comment();
        //sacar usuario logueado y setearlo al user_id del comentario 
        $user= \Auth::user();
        $comment->user_id=$user->id;
        $comment->video_id=$request->input('video_id');
        $comment->body=$request->input('body');
        $comment->created_at=new \DateTime('now');
        
        //guardar
        $comment->save();
        //redirigir
        return redirect()->route('detailVideo',$comment->video_id)->with('message','Comentario guardado con éxito');
    }
    
    //borrar comentario
    public function delete($commentId){
        //extraer usuario logueado
        $user= \Auth::user();
        //extraer comentario por el id
        $comment= Comment::find($commentId);
        
        //si el comentario es del usuario logueado o lo es el vídeo que se comenta
        if($comment->user_id==$user->id || $comment->video->user_id==$user->id){
            $comment->delete();
        }
        
        return redirect()->route('detailVideo',$comment->video_id)->with('message','Comentario borrado');
    }
    
}
